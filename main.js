var completeAll = document.getElementById('completeAll');
var addBtn = document.getElementById ('addMis');
var titleValue = document.getElementById('titleMis');
var newElem = document.getElementById('allList');

titleValue.onkeypress = checkEnt;

function checkEnt() {
	if (event.keyCode === 13) {
		checkSymb();
	}
 } // проверка на нажатие enter

addBtn.addEventListener ('click', checkSymb)
 
 function checkSymb() {
	  if (titleValue.value === '') {
    alert('Нужно что-то написать');
      } else {
    newElement();
        } 
 } // проверка написаны ли символы
 
 function newElement() {
    	var list = document.createElement('li');
    	list.innerText = titleValue.value;
	    var closeBtn = document.createElement('input');
	    closeBtn.type = 'button';
	    closeBtn.id = 'close';
        closeBtn.value = 'x';
	    closeBtn.setAttribute("onclick", "this.parentNode.parentNode.removeChild(this.parentNode);");
	    list.appendChild(closeBtn);
    	newElem.appendChild(list);
	    document.getElementById('titleMis').value = '';
  } // добавление нового задания в список и удаление по кнопке
 

 function deleteFunc() {
    while (newElem.firstChild) {
   newElem.removeChild(newElem.firstChild);
    }
 } 
deleteAll.addEventListener ('click', deleteFunc); // удаление всех заданий

function complAll() { 
	 var list = document.getElementsByTagName ('li');
	 for (i = 0; i < list.length; i++) {
		 var x = list[i];
		 x.className = 'complete';
     }
 }
completeAll.addEventListener ('click', complAll) // выполнение всех заданий


var del = document.querySelector('ul');
del.addEventListener('click', function(complOne) {
  if (complOne.target.tagName === 'LI') {
    complOne.target.classList.toggle('complete');
  }
}, false);  // выполнение только одного задания
